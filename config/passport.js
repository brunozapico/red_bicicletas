const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},
  function(email, password, done) {
    Usuario.findOne({ email: email }, function (err, usuario) {
      if (err) { return done(err); }
      if (!usuario) {
        return done(null, false, { message: 'Email incorrecto.' });
      }
      if (!usuario.validPassword(password)) {
        return done(null, false, { message: 'Contraseña incorrecta.' });
      }
      return done(null, usuario);
    });
  }
));

passport.serializeUser(function(user, done){
  done(null, user.email);
});

passport.deserializeUser(function(email, done){
  Usuario.findOne({ email: email}, function(err, usuario){
    done(err, usuario);
  });
});

module.exports = passport;