var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = (req, res) => {
    Bicicleta.find({}, (err, bicis) => {
        res.render('bicicletas/index', {bicis: bicis});
    });
};

exports.bicicleta_create_get = (req, res) => {
    res.render('bicicletas/create');
};

exports.bicicleta_create_post = (req, res) => {
    Bicicleta.create({code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]}, (err, nuevaBici) => {
        if(err){
            res.render('bicicletas/create', {errors: err.errors, bici: new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]})} );
        } else {
            res.redirect('/bicicletas');
        };
    });
};

exports.bicicleta_update_get = (req, res) => {
    Bicicleta.findById(req.params.id, (err, bicicleta) => {
        res.render('bicicletas/update', {errors:{}, bici: bicicleta});
    });
};

exports.bicicleta_update_post = (req, res) => {
    let update_values = {color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]};
    
    Bicicleta.findByIdAndUpdate(req.params.id, update_values, (err, bicicleta) => {
        if(err) {
            console.log(err);
            res.render('bicicletas/update', {errors: err.errors, bici: new Bicicleta({color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]})} );
        } else {
            res.redirect('/bicicletas');
            return;
        };
    });
};

exports.bicicleta_detele_post = (req, res) => {
    Bicicleta.findByIdAndDelete(req.params.id, err => {
        if(err) {
            next(err);
        } else {
            res.redirect('/bicicletas');
        };
    });
};