const passport = require('../config/passport');
const Usuario = require('../models/usuario');
const Token = require('../models/token');

module.exports = {
    login_get: (req, res, next) => {
        res.render('session/login');
    },
    login_post: (req, res, next) => {
        passport.authenticate('local', function (err, usuario, info) {
            if (err) return next(err);
            if (!usuario) return res.render('session/login', {
                info
            });
            req.login(usuario, function (err) {
                if (err) return next(err);
                return res.redirect('/');
            });
        })(req, res, next);
    },
    logout: (req, res, next) => {
        req.logOut();
        res.redirect('/');
    },
    forgotPassword_get: (req, res, next) => {
        res.render('session/forgotPassword');
    },
    forgotPassword_post: (req, res, next) => {
        Usuario.findOne({
            email: req.body.email
        }, function (err, usuario) {
            if (!usuario) return res.render('session/forgotPassword', {
                info: {
                    message: 'No existe ese email.'
                }
            });

            usuario.resetPassword(function (err) {
                if (err) return next(err);
                console.log('session/forgotPasswordMessage');
            });

            res.render('session/forgotPasswordMessage');
        });
    },
    resetPassword_get: function (req, res, next) {
        Token.findOne({
            token: req.params.token
        }, function (err, token) {
            if (!token) return res.status(400).send({
                type: 'not-verified',
                msg: 'No existe ese token.'
            });

            Usuario.findById(token._userId, function (err, usuario) {
                if (!usuario) return res.status(400).send({
                    msg: 'No existe un usuario asociado a ese token'
                });
                res.render('session/resetPassword', {
                    errors: {},
                    usuario: usuario
                });
            });
        });
    },
    resetPassword_post: function (req, res) {
        if (req.body.password != req.body.confirm_password) {
            res.render('session/resetPassword', {
                errors: {
                    confirm_password: {
                        message: 'No coinciden las contraseñas.'
                    }
                }
            });
            return;
        };
        Usuario.findOne({
            email: req.body.email
        }, function (err, usuario) {
            usuario.password = req.body.password;
            usuario.save(function (err) {
                if (err) {
                    res.render('session/resetPassword', {
                        errors: err.errors,
                        usuario: new Usuario({
                            email: req.body.email
                        })
                    });
                } else {
                    res.redirect('/login');
                };
            });
        })
    },
};