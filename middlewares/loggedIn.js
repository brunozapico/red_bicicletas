// asegurarse que el usuario esta logueado
module.exports = function loggedIn(req, res, next) {
    if (req.user) {
        next();
    } else {
        console.log('User sin loguearse');
        res.redirect('/login');
    };
};