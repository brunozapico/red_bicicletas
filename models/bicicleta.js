const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true }
    }
});

bicicletaSchema.methods.toString = () => {
    return `id: ${this._id} | color: ${this.color}`
};

// bicicletaSchema.statics.createInstance = function(color, modelo, ubicacion) {
//     return new this({
//         color: color,
//         modelo: modelo,
//         ubicacion: ubicacion
//     });
// };

// statics agrega directo al modelo
// bicicletaSchema.statics.allBicis = function(cb) {
//     return this.find({}, cb); // filtro vacio porque quiero todas -> {}
// };

// statico porque lo ponemos como un mensaje que mandamos directamente al modelo en bicicleta_test.spec.js linea 48 "Bicicleta.add(aBici, (err, newBici)"
// bicicletaSchema.statics.add = function(aBici, cb) {
//     this.create(aBici, cb);
// };

// bicicletaSchema.statics.findByCode = function(aCode, cb) {
//     return this.findOne({code: aCode}, cb);
// };

// bicicletaSchema.statics.removeByCode = function(aCode, cb) {
//     return this.deleteOne({code: aCode}, cb);
// };

module.exports = mongoose.model('Bicicleta', bicicletaSchema);