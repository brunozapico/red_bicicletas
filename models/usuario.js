const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Reserva = require('./reserva');
const Schema = mongoose.Schema;

const bcrypt = require('bcrypt');
const crypto = require('crypto');
const saltRounds = 10;

const Token = require('../models/token');
const mailer = require('../mailer/mailer');

let validateEmail = email => {
    let re = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    return re.test(email);
};

let usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true, // se quitan espacios vacios al principio y al final
        required: [true, 'El nombre es obligatorio.']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio.'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor, ingrese un email valido.'],
        match: [/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio'],
    },
    passwordResetTokek: String,
    passwordResertTokerExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

// librerias externas a mongoose incorporadas
usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario.'})

// antes del save se ejecuta la funcion
usuarioSchema.pre('save', function(next) {
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    };
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    let reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err) {
        if(err) { return console.log(err.message); };
        
        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificación de cuenta',
            text: `Hola,\n\n Por favor, para verificar su cuenta haga click en este link: \n` + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function(err) {
            if(err) { return console.log(err.message); };

            console.log(`Se ha enviado un email de verificación a: ${email_destination}.`);
        });
    });
};

usuarioSchema.methods.resetPassword = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err) {
        if(err) { return console.log(err.message); };

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de password de cuenta',
            text: `Hola,\n\n Por favor, para resetear el password de su cuenta haga click en este link: \n` + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function(err) {
            if(err) { return console.log(err.message); };

            console.log(`Se ha enviado un email para resetear el password a: ${email_destination}.`);
        });

        cb(null);
    });
};

module.exports = mongoose.model('Usuario', usuarioSchema);